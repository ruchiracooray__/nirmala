<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nirmala');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':XIU}@>`!zmOO9~uDIeg9|Rk>b<bk.uSC,XEWB+X-,o]mBrJa?xX8momb+9-I[C[');
define('SECURE_AUTH_KEY',  'oO:M?pVaD;poW@d!|jmluCz[->kE|oU!a?n$`I<.yQuX-N[Fhtbslw%J>6p|XG>E');
define('LOGGED_IN_KEY',    'B,ln^0l!O WYDHOGs||knhO1j+trxh6pq5~q>WZoj2.XwraZ{?-iz>UsT-R|/LeL');
define('NONCE_KEY',        'abt0Hzu`t>OX2xk3bn;P%|&b+W;8jXJ5X}bcq@L{F@KPmIt}X<#&i_IHhj$VGk{Y');
define('AUTH_SALT',        '54_hD$;l3P~,7/-TX!hg|Zk5VK,jU)faea+9;/Jy@I@ {gtnV{rvmA/wELZi@7e|');
define('SECURE_AUTH_SALT', ' ~qqy|U[oLCv.lR&STRd0~&<;_]%e$lw2|t-.SvL4xBz:7 5;Cp5[h_9N:BeOR,I');
define('LOGGED_IN_SALT',   '>?j>_|V-}^`,FK98[G~w`;S-=/K-JiFm1~3,`%k[;Ad+]xpsl+{+Ps&Q>>nNO6%Q');
define('NONCE_SALT',       '@,)KNc7,-_vU*FW!t8&|qBwR[ALH._YdOVIf-Ba0a_6~,kkWC*{jOs)$>uG8>AlS');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '7signs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
