module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    uglify: {
     
      build: {
        src: ['js/bootstrap.js','html5shiv.js'],
        dest: 'js/build/bootstrap.min.js'
      }
    },
    sass: {                              
        dist: {                           
          options: {                      
            style: 'compressed'
          },
          files: {                         
            'css/build/main.css': 'css/main.scss'
          }
        }
      },
bower_concat: {
  all: {
    dest: 'build/_bower.js',
    exclude: [
        'jquery',
        'modernizr'
    ],
    dependencies: {
      'underscore': 'jquery',
      'backbone': 'underscore',
      'jquery-mousewheel': 'jquery'
    },
    bowerOptions: {
      relative: false
    }
  }
},
    watch: {
            css: {
              files: 'css/main.scss',
              tasks: ['sass'],
              options: {
                livereload: true,
              },
            },
      },
      express:{
              all:{
                  options:{
                          port:9000,
                          hostname:'localhost',
                          base:['.'],
                          livereload:true


                  }

              }

      }



  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-bower-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express');
  // Default task(s).

  grunt.registerTask('default', ['uglify','sass','bower_concat']);
  grunt.registerTask('server', ['express','watch']);
};