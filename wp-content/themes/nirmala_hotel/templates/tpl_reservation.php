<?php

/*

Template Name:Reservation page

Theme Name: Nirmala hotel

Theme URI:  www.Nirmalahotel.com

Author  : Sevensigns.lk

Author URI: http://sevensigns.lk/

Description: Nimala Hotel website.

Version: 1.0

License: GNU General Public License v2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html



*/



?>

<?php get_header(); ?>


<!-- ########################## Contact page content ######################### -->

<div id="header-img">
    <img src="<?php echo get_template_directory_uri(); ?>/img/bg-2.jpg">
</div>

<!-- ########################## Home page content ######################### -->

<div class='container' id='homepage_content'>

<div class='logo-2'><div><img src="<?php echo get_template_directory_uri(); ?>/img/logo-2.jpg"></div></div>
<h1>Reservation</h1>


<div id='reservation'>
<div class="row">
	<div class="col-md-8">
		<div class="reserve-para">
			<h4>NIRMALA  HOTEL COLOMBO SRI LANKA</h4>
			<small>9, Edward Lane, Colombo 00300, Sri LankaNumber of rooms : 90</small>
			<br><small>94112597444</small>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
		</div>

		<div class="facilities content-box">
			<h2>FACILITIES OF NIRMALA HOTEL</h2>
		</div>
		<div class="facilities-cont">
			<div class="row">
				<div class='col-md-6'><p class='icon-tick'>Parking lot</p></div>
				<div class='col-md-6'><p class='icon-tick'>Access Satellite TV</p></div>
				<div class='col-md-6'><p class='icon-tick'>WiFi in Lobby Balcony or Terrace Outdoor</p></div>
				<div class='col-md-6'><p class='icon-tick'>Swimming pool</p></div>
				<div class='col-md-6'><p class='icon-tick'>Gym Spa</p></div>
			</div>
		</div>
	</div> <!-- end of .col-md-8 -->

	<div class="col-md-4">
		<div class="content-box contact-box-cont">

			  <img class='full-width-img' src="<?php echo get_template_directory_uri(); ?>/img/pattern-down.svg">
			<div id='enquery-box' class="contact_box">
				<h4>How Can We Help? </h4>
				<form role="form">
				  <div class="form-group">
				    <label class='icon-calendar1' for="email">Email address</label>
				    <input type="email" class="form-control" id="email">
				  </div>
				  <div class="form-group">
				    <label class='icon-calendar2' for="pwd">Check in</label>
				    <input id="datepicker1" type="" class="form-control" id="pwd">
				  </div>
				   <div class="form-group">
				    <label class='icon-calendar3' for="pwd">Check out</label>
				    <input id="datepicker2" type="" class="form-control" id="pwd">
				  </div>
				  <div class='form-btn'>
				  <button type="submit" class="btn btn-default">SEND INQUERY</button>
				  </div>
				</form>


			  </div>
			   <img class='full-width-img' src="<?php echo get_template_directory_uri(); ?>/img/pattern.svg">
		</div>

	<div class="res-offers content-box">
			<h2>NIRMALA HOTEL OFFERS</h2>

			<div class="row">
				<div class='ofr-pic col-md-6'><img src="<?php echo get_template_directory_uri(); ?>/img/ofr.jpg" alt='offer-pic'></div>
				<div class='ofr-details col-md-6'><h3>10%</h3><span> off</span>
					<h4>This March</h4>
					<a href="#">View</a>
				</div>
			</div>
		</div>
		
	</div> <!-- end of .col-md-4 -->

</div>
</div>
</div> <!-- end of container-->


<?php get_footer(); ?>