<?php

/*

Template Name:Booking page

Theme Name: Nirmala hotel

Theme URI:  www.Nirmalahotel.com

Author  : Sevensigns.lk

Author URI: http://sevensigns.lk/

Description: Nimala Hotel website.

Version: 1.0

License: GNU General Public License v2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html



*/



?>

<?php get_header(); ?>


<!-- ########################## Booking page content ######################### -->

<div id="header-img">
    <img src="<?php echo get_template_directory_uri(); ?>/img/bg-3.jpg">
</div>

<!-- ########################## Home page content ######################### -->

<div class='container' id='homepage_content'>

<div class='logo-2'><div><img src="<?php echo get_template_directory_uri(); ?>/img/logo-2.jpg"></div></div>
<h1>Booking</h1>


<div id='reservation'>
<div class="row">
	<div class="col-md-8">
		<div class="res-offers facilities content-box">
		<h2>Room Overview</h2>
			   <p>Room Overview

Donec euismod tempus turpis ut varius. Fusce mattis metus et nunc ultricies pellentesque. Nunc vitae diam magna, nec auctor sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
<br>
<br>
Etiam molestie molestie lorem, in aliquet ligula aliquet laoreet. Cras vehicula vulputate nibh, vitae egestas nibh egestas vitae.</p>
		</div>

		<div id='booking-cont'>
			<div id="standard" class="book-info">
				<h3>Standard Room</h3>
				<hr>
				<div class='booking-img'>
					<ul class="bxslider">
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					</ul>
				</div>
				<div class='row'>
					<div class='col-md-10'>
						<p>Located on the 3rd floor. Room  with ocean and Colombo skyline view, provided subject to availibility. All have a Queen size bed. All rooms offer an interconnect and Ideal for families.</p>
					</div>
					<div class='col-md-2'><a class='book-btn' href="">BOOK NOW</a></div>
				</div>
			</div>
			<!--end of #-->
		    <div id="superior" class="book-info">
				<h3>Superior Room</h3>
				<hr>
				<div class='booking-img'>
					<ul class="bxslider">
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					</ul>
				</div>
				<div class='row'>
					<div class='col-md-10'>
						<p>Located on the 3rd floor. Room  with ocean and Colombo skyline view, provided subject to availibility. All have a Queen size bed. All rooms offer an interconnect and Ideal for families.</p>
					</div>
					<div class='col-md-2'><a class='book-btn' href="">BOOK NOW</a></div>
				</div>
		    </div>
		    <!--end of #-->
		    <div id="deluxe" class="book-info">
				<h3>Deluxe Room</h3>
				<hr>
				<div class='booking-img'>
					<ul class="bxslider">
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					</ul>
				</div>
				<div class='row'>
					<div class='col-md-10'>
						<p>Located on the 3rd floor. Room  with ocean and Colombo skyline view, provided subject to availibility. All have a Queen size bed. All rooms offer an interconnect and Ideal for families.</p>
					</div>
					<div class='col-md-2'><a class='book-btn' href="">BOOK NOW</a></div>
				</div>
		    </div>
		    <!--end of #-->
		    <div id="oceanBalcony" class="book-info">
				<h3>Ocean View Balcony Suite</h3>
				<hr>
				<div class='booking-img'>
					<ul class="bxslider">
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					</ul>
				</div>
				<div class='row'>
					<div class='col-md-10'>
						<p>Located on the 3rd floor. Room  with ocean and Colombo skyline view, provided subject to availibility. All have a Queen size bed. All rooms offer an interconnect and Ideal for families.</p>
					</div>
					<div class='col-md-2'><a class='book-btn' href="">BOOK NOW</a></div>
				</div>
		    </div>
		    <!--end of #-->
		    <div id="oceanSuite" class="book-info">
				<h3>Ocean View Suite</h3>
				<hr>
				<div class='booking-img'>
					<ul class="bxslider">
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					</ul>
				</div>
				<div class='row'>
					<div class='col-md-10'>
						<p>Located on the 3rd floor. Room  with ocean and Colombo skyline view, provided subject to availibility. All have a Queen size bed. All rooms offer an interconnect and Ideal for families.</p>
					</div>
					<div class='col-md-2'><a class='book-btn' href="">BOOK NOW</a></div>
				</div>
		    </div>
		    <!--end of #-->
		    <div id="spa" class="book-info">
				<h3>Ocean View Spa Suite</h3>
				<hr>
				<div class='booking-img'>
					<ul class="bxslider">
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/img/room-1.jpg" /></li>
					</ul>
				</div>
				<div class='row'>
					<div class='col-md-10'>
						<p>Located on the 3rd floor. Room  with ocean and Colombo skyline view, provided subject to availibility. All have a Queen size bed. All rooms offer an interconnect and Ideal for families.</p>
					</div>
					<div class='col-md-2'><a class='book-btn' href="">BOOK NOW</a></div>
				</div>
		    </div>
		    <!--end of #-->
		</div>

		<div class="facilities content-box">
			<h2>Room Facilities</h2>
		</div>
		<div class="room-facilities-cont">
			<div class="row">
				<div class='col-md-3'>
					<h5 id='cable-tv'>Cable Televition</h5>
					<p>Lorem Ipsum is simply</p>
				</div>

				<div class='col-md-3'>
					<h5 id='wi-fi'>Unlimited Wi-Fi</h5>
					<p>Lorem Ipsum is simply</p>
				</div>

				<div class='col-md-3'>
					<h5 id='air-condition'>Air Conditioning</h5>
					<p>Lorem Ipsum is simply</p>
				</div>

				<div class='col-md-3'>
					<h5 id='room-service'>24 Hour Room Service</h5>
					<p>Lorem Ipsum is simply</p>
				</div>
			</div>
		</div>
	</div> <!-- end of .col-md-8 -->

	<div class="col-md-4">
			<ul class="side-nav" id="sideNav">
			    <li id="standardNav"><a class='book-link start' data-target="standard" title="standard" href='#a'>Standard Room</a></li>
			    <li id="superiorNav"><a class='book-link' data-target="superior" title="superior" href="#b">Superior Room</a></li>
			    <li id="deluxeNav"><a class='book-link' data-target="deluxe" title="deluxe" href="#c">Deluxe Room</a></li>
			    <li id="balconyNav"><a class='book-link' data-target="oceanBalcony" title="oceanBalcony" href="#d">Ocean View Balcony Suite</a></li>
			    <li id="suiteNav"><a class='book-link' data-target="oceanSuite" title="oceanSuite" href="#e">Ocean View Suite</a></li>
			    <li id="spaNav"><a class='book-link' data-target="spa" title="spa"  href="#f">Ocean View Spa Suite</a></li>
			</ul>


		<div class="content-box contact-box-cont">
			  <img class='full-width-img' src="<?php echo get_template_directory_uri(); ?>/img/pattern-down.svg">
			<div id='enquery-box' class="contact_box">
				<h4>How Can We Help? </h4>
				<form role="form">
				  <div class="form-group">
				    <label class='icon-calendar1' for="email">Email address</label>
				    <input type="email" class="form-control" id="email">
				  </div>
				  <div class="form-group">
				    <label class='icon-calendar2' for="pwd">Check in</label>
				    <input id="datepicker1" type="" class="form-control" id="pwd">
				  </div>
				   <div class="form-group">
				    <label class='icon-calendar3' for="pwd">Check out</label>
				    <input id="datepicker2" type="" class="form-control" id="pwd">
				  </div>
				  <div class='form-btn'>
				  <button type="submit" class="btn btn-default">SEND INQUERY</button>
				  </div>
				</form>


			  </div>
			   <img class='full-width-img' src="<?php echo get_template_directory_uri(); ?>/img/pattern.svg">
		</div>

	<div class="res-offers content-box">
			<h2>NIRMALA HOTEL OFFERS</h2>

			<div class="row">
				<div class='ofr-pic col-md-6'><img src="<?php echo get_template_directory_uri(); ?>/img/ofr.jpg" alt='offer-pic'></div>
				<div class='ofr-details col-md-6'><h3>10%</h3><span> off</span>
					<h4>This March</h4>
					<a href="#">View</a>
				</div>
			</div>
		</div>
		
	</div> <!-- end of .col-md-4 -->

</div>
</div>
</div> <!-- end of container-->


<!--booking page js -->
<script type="text/javascript">
	$('.book-link:first').addClass('active_target');
	$('.book-info:first').addClass('active_data');

       $(document).ready(function() {

				$('.book-link').click(function(e){
					$('.book-link:first').removeClass('active_target');
					 $(this).siblings().removeClass('active_target').addClass('active_target');
	        
			        //get the target id name.
			        var target = $(this).data('target');

			        $('.book-info').each(function(){
						$(this).fadeOut(200);
			        })

			        $('#'+target).fadeIn(1000);

			        e.preventDefault();

		        });
			 

				jQuery('.bxslider').bxSlider({
				  mode: 'fade',
				  captions: true
				});
			});



</script>
	<!--end of booking page js -->

<?php get_footer(); ?>