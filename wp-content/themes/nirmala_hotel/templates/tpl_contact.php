<?php

/*

Template Name:Contact page

Theme Name: Nirmala hotel

Theme URI:  www.Nirmalahotel.com

Author  : Sevensigns.lk

Author URI: http://sevensigns.lk/

Description: Nimala Hotel website.

Version: 1.0

License: GNU General Public License v2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html



*/



?>

<?php get_header(); ?>


<!-- ########################## Contact page content ######################### -->

<div id="header-img">
    <img src="<?php echo get_template_directory_uri(); ?>/img/bg1.jpg">
</div>

<!-- ########################## Home page content ######################### -->

<div class='container' id='homepage_content'>

<div class='logo-2'><div><img src="<?php echo get_template_directory_uri(); ?>/img/logo-2.jpg"></div></div>
<h1>Contact Us</h1>
<p class='welcome-para'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>

<div id='contact'>
<div class="row">
	<div class="col-md-7">
		<div class="get-in-touch contact-cont">
			<h2>Get In Touch</h2>
			<form class="form-horizontal" method='post' action='<?php echo get_template_directory_uri();?>/mail.php'>
                    <div class="">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="">Name</label>
                                <div class=""><input name='name' id='name' type="text" class="form-control"></div>
                            </div>
                            <div class="col-md-6">
                                <label class="">Phone</label>
                                <div class=""><input name='tel' id='tel' type="text" class="form-control"></div>
                            </div>
                        </div>
                    </div>

                    <div class="">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="">Subject</label>
                                <div class=""><input name='subject' id='subject' type="text" class="form-control"></div>
                            </div>
                            <div class="col-md-6">
                                <label class="">Email</label>
                                <div class=""><input name='email' id='email' type="email" class="form-control"></div>
                            </div>  
                            <div class="col-md-12">
                            <label class="">Message</label>
                            <div class=""><textarea name='message' id='message' rows="5" class="form-control"></textarea></div>
                            </div>
                        </div>
                    </div>

                    <div class='row'>
						<div class='col-md-12'>
		                    <div class="contact-btn">
		                          <button type="submit" class="btn">SUBMIT</button>
		                    </div>
	                    </div>
                    </div>
                  <!--===================end of form-group===================-->
                 </form>
		</div>
	</div>

	<div class="col-md-5">
		<div class="row">
			<div class="col-md-6">
				<div class="contact-cont">
					<h2>Our Location</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's </p>
				</div>
			</div>
			<div class="col-md-6">
				<!--This is a blank div-->
			</div>
		</div>

		<div class="row">
			<div class='contact-cont-2'>
			<div class="col-md-6">
				<div class="contact-cont">
					<h2>Have Questions?</h2>
					<p>Lorem Ipsum is simply dummy text of the </p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="contact-cont">
					<h2>Follow Us</h2>
					<ul class='social'>
						<li><a class='icon-black-social-twitter' href=""></a></li>
						<li><a class='icon-black-social-facebook' href=""></a></li>
						<li><a class='icon-black-social-google-plus' href=""></a></li>
					</ul>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>
</div>
</div> <!-- end of container-->


<?php get_footer(); ?>