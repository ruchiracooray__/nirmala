<!-- footer -->
<footer class="footer" role="contentinfo">
<div class='container'>
	<div class='row'>
        <div class='col-md-3 col-sm-3'>
            <div class='footer_our_services'>
                <?php if(is_active_sidebar( 'our_services' )){
                dynamic_sidebar('our_services');
               }; ?> 
            </div>
        </div>

        <div class='col-md-3 col-sm-3'>
            <div class='quick_links'>
                <?php if(is_active_sidebar( 'quick_links' )){
                dynamic_sidebar('quick_links');
               }; ?> 
            </div>
        </div>

        <div class='col-md-3 col-sm-3'>
            <div class='footer_contact_us'>
	           <!-- <h5>About Us</h5> -->

	           <?php if(is_active_sidebar( 'about_us' )){
                dynamic_sidebar('about_us');
               }; ?> 
	                          <!-- <ul>
	                               <li class='icon-icon-map'><?php //echo (the_field('address','19')); ?></li>
	                               <li class='icon-icon-email'><?php //echo (the_field('email','19')); ?></li>
	                               <li class='icon-icon-phone'><?php //echo (the_field('phone','19')); ?></li>
	                               <li class='icon-icon-web'><?php //echo (the_field('web_address','19')); ?></li>
	                           </ul> -->
            </div>


            <div class='social'>
	<h5>FOLLOW US</h5>
	<ul class='social'>
		<li><a class='icon-round-social-twitter' href=""></a></li>
		<li><a class='icon-round-social-facebook' href=""></a></li>
		<li><a class='icon-round-social-googleplus' href=""></a></li>
	</ul>
            </div>
        </div>

        <div class='col-md-3 col-sm-3'>
            <div class='stay_in_touch'>
            	<?php if(is_active_sidebar( 'stay_in_touch' )){
                dynamic_sidebar('stay_in_touch');
               }; ?> 
            </div>

            <div class='newsletter'>
            	<h5>NEWSLETTER</h5>
            	<p>Signup for the monthly newsletter to get usefull information for your email</p>
            	<div><input class='footer-text-box' type='text' /> <input type='submit' class='footer-button' value='Sign up'></div>
            </div>
        </div>

    </div><!--end of main row-->
    <div id='footer_row2' class='row'>
            <div class='col-md-6 col-sm-6' >
                <div class='footer_btm_list'>
                <ul>
                    <li><a href="">Terms and Conditions</a></li>
                    <li><a href="">Privacy</a></li>
                    <li><a href="">Disclaimer</a></li>
                    <li><a href="">Useful links</a></li>
                </ul>
                </div>
            </div>
            <!-- <div class='col-md-3'></div> -->
            <div class='col-md-6 '>
				<div class='row'>
					<div class="col-md-4"></div>
					<div class="col-md-8">
						<div class="copyright-cont">
		                <div class='copyright'>
		                <!-- copyright -->
		                <p>
		                    &copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. <?php _e('Powered by', 'html5blank'); ?>
		                    <a href="http://www.sevensigns.lk" title="WordPress">Sevensigns.lk</a> 
		                </p>
		                </div>
		                <!-- /copyright -->
		                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-2.jpg">
					</div>
					</div>
				</div>
            </div>
        </div>
			</div><!--/container-->
			</footer>
			<!-- /footer -->

			<!-- /container -->

		<!-- /container full width -->

<?php wp_footer(); ?>

<script type="text/javascript">
 $(document).ready(function(){

    setTimeout(function(){
        $("#hideAll").fadeOut(4000)
    }, 500 );

});

 //$(window).load(function() {  document.getElementById("hideAll").style.display = "none"; });
 </script> 

		<!-- analytics -->
<script type="text/javascript">



jQuery(function(){

	var p=jQuery('#content-slider').responsiveSlides({
		height:572,           						// slides conteiner height
		background:'#fff',    				// background color and color of overlayer to fadeout on init
		autoStart:true,       					// boolean autostart
		startDelay:0,         					// start whit delay
		effectInterval:5000,  			// time to swap photo
		effectTransition:1000,			// time effect
		pagination:[
			{
				active:true,      			// activate pagination
				inner:true,       				// pagination inside or aouside slides conteiner
				position:'B_L',   		/* 
				                  							pagination align:
				                  								T_L = top left
				                  								T_C = top center
				                  								T_R = top right
				                  					
				                  								B_L = bottom left
				                  								B_C = bottom center
				                  								B_R = bottom right
				                  						*/
				margin:10,        				// pagination margin
				dotStyle:'',      			// dot pagination class style
				dotStyleHover:'', 		// dot pagination class hover style
				dotStyleDisable:''		// dot pagination class disable style
			}
		]
	});

});
</script>

<script type="text/javascript">



</script>

	<script>
		/*(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');*/
		
jQuery(document).ready(function() {
 
  var owl = jQuery("#owl-demo");
 
  owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      pagination:false,
      autoPlay: 3000
  });
 
  // Custom Navigation Events
  jQuery(".next").click(function(){
    owl.trigger('owl.next');
  })
  jQuery(".prev").click(function(){
    owl.trigger('owl.prev');
  })
  jQuery(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })
 

});

 jQuery(function() {
    $("#datepicker1").datepicker();
    $("#datepicker2").datepicker();
  });


		</script>
	
	


	<script type="text/javascript">     
          function GoogleLanguageTranslatorInit() { new google.translate.TranslateElement({pageLanguage: 'en', autoDisplay: false, gaTrack: true, gaId: ''}, 'google_language_translator');}</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=GoogleLanguageTranslatorInit"></script>
		<!-- analytics -->
		
		<script>if(!Modernizr.svg){

var i=document.getElementsByTagName("img"),j,y;for(j=i.length;j--;)y=i[j].src,y.match(/svg$/)&&(i[j].src=y.slice(0,-3)+"png")}

</script>

	</body>
</html>
